from zeroinstall.injector import namespaces, model
import os, shutil, sys
import rox
from rox import saving
from xml.dom import minidom
import urllib2
import tempfile, shutil

addapp_uri = "http://rox.sourceforge.net/2005/interfaces/AddApp"

class NoMain(Exception):
	pass

class AppLauncher(saving.Saveable):
	def __init__(self, iface_cache, stores, sels):
		self.uri = sels.interface
		self.iface_cache = iface_cache
		self.stores = stores
		self.icon = None
		self._icon_tmp = None

		self.impl = sels.selections[self.uri]
		if not self.impl:
			raise Exception("Failed to select an implementation of %s" % self.uri)

	def save_to_file(self, path):
		os.mkdir(path)
		assert "'" not in self.uri
		iface = self.iface_cache.get_interface(self.uri)

		apprun = os.fdopen(os.open(os.path.join(path, 'AppRun'), os.O_CREAT | os.O_WRONLY, 0777), 'w')
		if self.uri == 'http://rox.sourceforge.net/2005/interfaces/AddApp':
			# Funky special case: AddApp behaves differently when run through AppRun
			apprun.write("""#!/bin/sh
if [ "$*" = "--versions" ]; then
  exec 0launch -gd '%s'
elif [ "$*" = "" ]; then
  exec 0launch '%s' --prompt
elif [ "$*" = "--help" ]; then
  exec 0launch '%s' --show-help '%s'
else
  exec 0launch '%s' -- "$@"
fi""" % (self.uri, self.uri, addapp_uri, self.uri, self.uri))
		else:
			if len(iface.get_metadata(namespaces.XMLNS_IFACE, 'needs-terminal')):
				xterm = addapp_uri + ' --run-in-terminal '
			else:
				xterm = ''
			apprun.write("""#!/bin/sh
if [ "$*" = "--versions" ]; then
  exec 0launch -gd '%(uri)s'
elif [ "$*" = "--help" ]; then
  exec 0launch '%(addapp)s' --show-help '%(uri)s'
else
  exec 0launch %(xterm)s'%(uri)s' "$@"
fi""" % {'uri': self.uri, 'addapp': addapp_uri, 'xterm': xterm})

		apprun.close()
		if self.icon:
			shutil.copyfile(self.icon, os.path.join(path, '.DirIcon'))

		doc = self.get_appinfo(iface)
		info_file = file(os.path.join(path, 'AppInfo.xml'), 'w')
		doc.writexml(info_file)
		info_file.close()

		if self.is_applet(iface):
			appletrun = os.fdopen(os.open(os.path.join(path, 'AppletRun'),
						os.O_CREAT | os.O_WRONLY, 0777), 'w')
			appletrun.write("""#!/bin/sh
exec 0launch --main=AppletRun '%s' "$@"
""" % self.uri)
			appletrun.close()

	def get_appinfo(self, iface):
		appdir = self.get_appdir(self.impl)
		if appdir:
			appinfo = os.path.join(appdir, 'AppInfo.xml')
		else:
			appinfo = None

		if appinfo and os.path.exists(appinfo):
			doc = minidom.parse(appinfo)
		else:
			doc = minidom.parseString("<?xml version='1.0'?>\n<AppInfo/>")
		root = doc.documentElement
		appmenu = self.get_item(root, 'AppMenu')
		self.add_item(appmenu, '--versions', {'en': 'Versions...'})
		self.add_item(appmenu, '--help', {'en': 'Help'})
		if not root.getElementsByTagName('Summary'):
			summary = doc.createElement('Summary')
			summary.appendChild(doc.createTextNode('%s -- %s' % (iface.name, iface.summary)))
			root.appendChild(summary)
		for summary in root.getElementsByTagName('Summary'):
			summary.appendChild(doc.createTextNode('\n(%s)' % iface.uri))

		for about in root.getElementsByTagName('About'):
			for version in about.getElementsByTagName('Version'):
				if not version.childNodes: continue
				for c in version.childNodes:
					version.removeChild(c)
				version.appendChild(doc.createTextNode('(launcher)'))
		return doc
	
	def add_item(self, menu, option, labels):
		doc = menu.ownerDocument
		item = doc.createElement('Item')
		kids = menu.childNodes
		if kids:
			menu.insertBefore(item, kids[0])
		else:
			menu.appendChild(item)
		item.setAttribute('option', option)

		for lang in labels:
			label = doc.createElement('Label')
			item.appendChild(label)
			label.setAttribute('xml:lang', lang)
			label.appendChild(doc.createTextNode(labels[lang]))
	
	def get_item(self, parent, name):
		items = parent.getElementsByTagName(name)
		if items:
			return items[0]
		item = parent.ownerDocument.createElement(name)
		parent.appendChild(item)
		return item
	
	def get_appdir(self, impl):
		impl_path = impl.local_path or self.stores.lookup_any(impl.digests)
		main = impl.main
		if not main:
			return None
		assert not main.startswith('/'), main
		return os.path.dirname(os.path.join(impl_path, main))

	def is_applet(self, iface):
		appdir = self.get_appdir(self.impl)
		if appdir:
			applet_run = os.path.join(appdir, 'AppletRun')
			return os.path.exists(applet_run)
		else:
			return False

	def get_icon(self):
		"""Sets self.icon to a filename of an icon for the app (downloading it first), or None."""
		iface = self.iface_cache.get_interface(self.uri)
		path = self.iface_cache.get_icon_path(iface)
		if path:
			self.icon = path
		return
	
	def delete_icon(self):
		"""Remove the temporary icon file, if any"""
		self._icon_tmp = None
