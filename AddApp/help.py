from zeroinstall import helpers
from zeroinstall.injector import policy, handler
import rox, os
from rox import filer

def show_help(uri):
	config = policy.load_config(handler.Handler())
	def get_implementation_path(sel):
		return sel.local_path or config.iface_cache.stores.lookup_any(sel.digests)

	sels = helpers.ensure_cached(uri)

	sel = sels.selections[uri]
	path = get_implementation_path(sel)

	# Get the help directory from the feed meta-data if possible
	help = None
	docdir = sel.attrs.get('doc-dir', None)
	if docdir:
		help = os.path.join(path, docdir)

	# Nothing in the feed. See if there's a Help directory next to the executable
	if help is None:
		main = sels.commands[0].path
		assert main
		help = os.path.join(path, os.path.dirname(main), 'Help')

	if not os.path.isdir(help):
		rox.alert('Sorry, this program has no documentation\n\n(%s directory missing)' % help)
	else:
		filer.open_dir(help)
