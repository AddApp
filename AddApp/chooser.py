import rox
from rox import g, loading
import codecs

class Chooser(rox.Dialog, loading.XDSLoader):
	def __init__(self):
		rox.Dialog.__init__(self)
		loading.XDSLoader.__init__(self, ['text/x-moz-url'])

		vbox = g.VBox(False, 2)
		self.vbox.pack_start(vbox, True, True, 0)
		vbox.set_border_width(10)

		message = g.Label('Enter the URI of a program available through the Zero Install\n'
				  'Injector system, or drag a link from a web browser into this\n'
				  'window.')
		self.set_has_separator(False)
		vbox.pack_start(message)

		self.uri = g.Entry()
		self.uri.set_text('http://rox.sourceforge.net/2005/interfaces/ROX-Filer')
		vbox.pack_start(self.uri)
		self.uri.set_activates_default(True)
		
		self.vbox.show_all()

		fetching = g.Label('Checking interface URI...')
		vbox.pack_start(fetching)
		
		self.add_button(g.STOCK_CANCEL, g.RESPONSE_CANCEL)
		self.add_button(g.STOCK_ADD, g.RESPONSE_OK)
		self.set_default_response(g.RESPONSE_OK)

	def xds_load_uris(self, uris):
		assert uris
		if len(uris) > 1:
			rox.error("Can't handle multiple URIs at once! (got %s)" % repr(uris))
			return
		self.uri.set_text(uris[0])
	
	def xds_load_from_selection(self, selection, leafname = None):
		if selection.data is None:
			g.gdk.beep()	# Load aborted
			return
		data = codecs.utf_16_decode(selection.data)[0]
		uri = data.split('\n')[0]
		self.xds_load_uris([uri])
