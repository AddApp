import os

def find_in_path(prog):
	"""Search $PATH for prog.
	If prog is an absolute path, return it unmodified.
	@param prog: name of executable to find
	@return: the full path of prog, or None if not found
	"""
	if os.path.isabs(prog): return prog
	for d in os.environ['PATH'].split(':'):
		path = os.path.join(d, prog)
		if os.path.isfile(path):
			return path
	return None

def run_in_terminal(args):
	terminal_emulators = ['x-terminal-emulator', 'xterm', 'konsole']
	for xterm in terminal_emulators:
		full = find_in_path(xterm)
		if full:
			os.execvp(full, [full, '-e', '0launch', '--'] + list(args))

	import rox
	rox.croak("Can't run this program because it needs a terminal emulator and "
		  "you don't seem to have one!\n\nI looked for one of these:\n\n" + '\n'.join(terminal_emulators))
